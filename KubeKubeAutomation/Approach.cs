﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace KubeKubeAutomation
{
    internal class Approach
    {
        private static Random random = new Random();
        private static int standardRed { get; set; }
        private static int standardGreen { get; set; }
        private static int standardBlue { get; set; }
        private static double standardAlpha { get; set; }
        private static bool boxFound { get; set; }
        private static string colorStandardString { get; set; }

        public static void ObjectBuildAuto()
        {
            Random random = new Random();
            IWebDriver driver = new FirefoxDriver();
            driver.Url = "http://106.186.25.143/kuku-kube/en-3/";


            var startButton = driver.FindElement(By.TagName("button"));
            startButton.Click();
            bool gameOver = false;
            int round = 0;
            while (true)
            {
                round++;
                long startTime = DateTime.Now.Ticks;

                boxFound = false;

                var boxes = driver.FindElement(By.Id("box")).FindElements(By.TagName("span"));

                List<Box> boxList = new List<Box>();
                foreach (var box in boxes)
                {
                    string values = box.GetCssValue("background-color");


                    int start = values.IndexOf("(") + 1;
                    int end = values.LastIndexOf(")") - start;

                    string substring = values.Substring(start, end);


                    char delimiter = Char.Parse(",");
                    string[] rgbaValues = substring.Split(delimiter);


                    Box realBox = new Box(Int32.Parse(rgbaValues[0]), Int32.Parse(rgbaValues[1]),
                        Int32.Parse(rgbaValues[2]),
                        Double.Parse(rgbaValues[3]), box);
                    boxList.Add(realBox);
                }

                BuildColorStandard(boxList);

                if (!boxFound)
                {


                    FindDifferentBox(boxList);
                    long endResult = DateTime.Now.Ticks;
                    Console.WriteLine("Round {0} solved in {1} ms.", round, endResult - startTime);
                }


                try
                {
                    var restartButton = driver.FindElement(By.TagName("button"));
                }
                catch (Exception exception)
                {
                    if (exception is NoSuchElementException)
                    {
                        gameOver = true;
                    }
                }

            }
        }

        private static void FindDifferentBox(List<Box> boxList)
        {
            int looped = 0;
            //while (!boxFound)
            //{
            looped++;
            //Slow performance Score of 21 pm first run
            Box box = boxList.Find(x => x.Red != standardRed);

            if (box.Green != standardGreen || box.Blue != standardBlue || box.Alpha != standardAlpha)
            {
                box.WebElementObject.Click();
                boxFound = true;
            }

            //Console.WriteLine("Looped Find Box: {0} times.", looped);
            //Not much better... 20 on first try
            //foreach(Box box in boxList)
            //{
            //    if (box.Red != standardRed)
            //    {
            //        if (box.Green != standardGreen || box.Blue != standardBlue || box.Alpha != standardAlpha)
            //        {
            //            box.WebElementObject.Click();
            //            boxFound = true;
            //            break;
            //        }
            //    }
            //}
            //}


        }

        /**
         * Builds color standard from boxes
         */

        private static void BuildColorStandard(List<Box> boxList)
        {
            
            //Random sampling and remove from list
            Box firstBox = boxList.ElementAt(random.Next(0, boxList.Count - 1));
            boxList.Remove(firstBox);
            Box secondBox = boxList.ElementAt(random.Next(0, boxList.Count - 1));
            boxList.Remove(secondBox);

            //Comparison
            bool result = firstBox.CompareBox(secondBox);

            //if same, make standard
            if (result)
            {
                standardRed = firstBox.Red;
                standardBlue = firstBox.Blue;
                standardGreen = firstBox.Green;
                standardAlpha = firstBox.Alpha;
            }
            else
            {
                //Third sampling from list of boxes
                Box thirdBox = boxList.ElementAt(random.Next(0, boxList.Count - 1));
                boxList.Remove(thirdBox);

                //Find deviation
                if (firstBox.CompareBox(thirdBox))
                {

                    secondBox.WebElementObject.Click();
                    boxFound = true;
                    Console.WriteLine("Found during sampling");
                }
                else if (secondBox.CompareBox(thirdBox))
                {
                    firstBox.WebElementObject.Click();
                    boxFound = true;
                    Console.WriteLine("Found during sampling");
                }

            }

        }


        /**
         * Simplified version.  Minimal object creation to save resources and time
         * 
         */
        public static void LessObjectsMoreCowbell()
        {
            // Maintained Random
            Random random = new Random();
            IWebDriver driver = new FirefoxDriver();
            driver.Url = "http://106.186.25.143/kuku-kube/en-3/";


            var startButton = driver.FindElement(By.TagName("button"));
            startButton.Click();
            //bool gameOver = false;
            int round = 0;
            while (true)
            {
                round++;
                long startTime = DateTime.Now.Ticks;

                boxFound = false;

                var boxes = driver.FindElement(By.Id("box")).FindElements(By.TagName("span"));
                
                //Random sampling for first and second to build a standard
                int firstSampleIndex = random.Next(0, boxes.Count - 1);
                int secondSampleIndex = random.Next(0, boxes.Count - 1);

                //if standard exists
                if (boxes[firstSampleIndex].GetCssValue("background-color")
                    .Equals(boxes[secondSampleIndex].GetCssValue("background-color")))
                {
                    //Find the deviation via list.find()
                    List<IWebElement> elementList = boxes.ToList();
                    (elementList.Find(
                        x =>
                            x.GetCssValue("background-color") !=
                            elementList.ElementAt(firstSampleIndex).GetCssValue("background-color"))).Click();
                    long endResult = DateTime.Now.Ticks;
                    Console.WriteLine("Round {0} solved in {1} ms.", round, endResult - startTime);
                }
                else
                {
                    //no standard found because samples are different
                    int thirdSampleIndex = random.Next(0, boxes.Count - 1);
                    
                    //Compare third sample to determine deviation
                    if (boxes[firstSampleIndex].GetCssValue("background-color")
                        .Equals(boxes[thirdSampleIndex].GetCssValue("background-color")))
                    {
                        boxes[secondSampleIndex].Click();
                        
                        Console.WriteLine("Found during sampling");
                    }
                    else if (boxes[secondSampleIndex].GetCssValue("background-color")
                        .Equals(boxes[thirdSampleIndex].GetCssValue("background-color")))
                    {
                        boxes[firstSampleIndex].Click();
                        Console.WriteLine("Found during sampling");
                    }
                }
            }
        }
    }
}
