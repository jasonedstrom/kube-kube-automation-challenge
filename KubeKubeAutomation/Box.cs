﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace KubeKubeAutomation
{
    class Box
    {
        public int Red { get; set; }
        public int Green { get; set; }
        public int Blue { get; set; }
        public double Alpha { get; set; }
        public IWebElement WebElementObject{ get; set; }

        public Box()
        {
            Red = 0;
            Green = 0;
            Blue = 0;
            Alpha = 0;
            
        }

        public Box(int red, int green, int blue, double alpha, IWebElement webelement)
        {
            Red = red;
            Blue = blue;
            Green = green;
            Alpha = alpha;
            WebElementObject = webelement;
        }

        //Compare boxes
        public bool CompareBox(Box otherBox)
        {
            bool redMatch = false;
            bool greenMatch = false;
            bool blueMatch = false;
            bool alphaMatch = false;

            if (this.Red == otherBox.Red)
            {
                redMatch = true;
            }

            if (this.Green == otherBox.Green)
            {
                greenMatch = true;
            }

            if (this.Blue == otherBox.Blue)
            {
                blueMatch = true;
            }

            if (this.Alpha == otherBox.Alpha)
            {
                alphaMatch = true;
            }

            if (redMatch && blueMatch && greenMatch && alphaMatch)
            {
                return true;
            } else
            {
                return false;
            }

        }

    }
}
